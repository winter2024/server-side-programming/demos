from flask import Flask, abort, redirect, url_for, render_template, request
from post import Post
from blog_app.forms import postform
import os

app = Flask(__name__)
app.secret_key = os.environ['FLASK_SECRET']
