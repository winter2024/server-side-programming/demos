from blog_app.post import Post


class FakeDB():
    def __init__(self):
        self.posts = [
            {
            "author": "ddubois",
            "id": "1",
            "tag_line": "testing",
            "text": "this is a test",
            "title": "first post"
            },
            {
            "author": "ddubois",
            "id": "2",
            "tag_line": "testing",
            "text": "this is a test",
            "title": "test post"
            }
        ]

    def get_posts(self):
        return self.posts

    def get_post(self, id):
        return None

    def get_post_by_title(self, title):
        return [post for post in self.posts if post.title == title][0]

    def create_post(self, post):
        if not isinstance(post, Post):
            raise TypeError()

        self.posts.append(post)
        pass

    def close(self):
        return None
