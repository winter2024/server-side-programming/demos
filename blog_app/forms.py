from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, EmailField, PasswordField
from wtforms.validators import DataRequired


class postform(FlaskForm):
    title = StringField('title', validators=[DataRequired()])
    id = IntegerField('id', validators=[DataRequired()])
    author = StringField('author', validators=[DataRequired()])
    text = StringField('text', validators=[DataRequired()])
    tag_line = StringField('tag line', validators=[DataRequired()])


class SignupForm(FlaskForm):
    email = EmailField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    name = StringField('name', validators=[DataRequired()])

class LoginForm(FlaskForm):
    email = EmailField('email')
    password = PasswordField('password')