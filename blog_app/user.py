from flask_login import UserMixin
class User(UserMixin):
    def __init__(self, id, email, name, pwd):

        if not isinstance(email, str):
            raise TypeError()
        if not isinstance(pwd, str):
            raise TypeError()
        if not isinstance(name, str):
            raise TypeError()
        
        self.email = email
        self.name = name
        self.pwd = pwd
        self.id = id
