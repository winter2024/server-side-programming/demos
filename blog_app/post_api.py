from flask import Blueprint, abort, jsonify, request
from .DB import get_db
from .post import Post

bp = Blueprint("posts_api", __name__, url_prefix="/api/posts/")


@bp.route('',methods=['GET','POST'])
def get_posts():
    if request.method == 'POST':
        data = request.json
        post = Post.from_json(data)
        get_db().create_post(post)
        
    if request.args:
        title = request.args.get("title")
        if title: 
            post = get_db().get_post_by_title(title)
            if post:
                return jsonify(post.__dict__)
            else:
                abort(404)
        else:
            abort(400)
    posts = get_db().get_posts()
    json_posts = [post.__dict__ for post in posts]
    return jsonify(json_posts)
