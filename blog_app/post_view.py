from flask import Blueprint, abort, redirect, render_template, request, url_for

from blog_app.forms import postform
from blog_app.DB import get_db
from blog_app.post import Post

bp = Blueprint('posts', __name__, url_prefix='/posts/')


@bp.route('')
def get_posts():
    posts = get_db().get_posts()
    if posts is None or len(posts) == 0:
        abort(404)

    return render_template("posts.html", posts=posts)


@bp.route('/<int:post_id>/')
def show_post(post_id):
    # for post in posts:
    #     if post_id == post.id:
    #         return f'{post}'
    post = get_db().get_post(post_id)

    if post is None:
        return redirect(url_for('get_posts'))

    return f'{post}'


@bp.route('/new/', methods=['POST', 'GET'])
def create_post():
    form = postform()
    if request.method == 'POST' and form.validate_on_submit():

        new_post = Post(form.title.data, form.id.data,
                        form.author.data, form.text.data, form.tag_line.data)
        get_db().create_post(new_post)

        return redirect(url_for('show_post', post_id=new_post.id))

    return render_template('new_post.html', form=form)
