from flask import Blueprint, render_template, request

from blog_app.forms import SignupForm
from blog_app.user import User

bp = Blueprint('home', __name__, url_prefix='/')


@bp.route('')
def index():
    return render_template("home.html")
