import os
import click
from flask import Flask
from flask_login import LoginManager
from blog_app.DB import close_db, get_db


def create_app(test_config=False):
    app = Flask(__name__, instance_relative_config=True)
    app.secret_key = os.environ['FLASK_SECRET']
    app.testing = False

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        get_db().get_user_by_id(user_id)

    from .home_view import bp as home_bp
    app.register_blueprint(home_bp)

    from .post_view import bp as post_bp
    app.register_blueprint(post_bp)

    from .post_api import bp as api_bp
    app.register_blueprint(api_bp)

    from .auth_views import bp as auth_bp
    app.register_blueprint(auth_bp)

    app.teardown_appcontext(close_db)
    return app


@click.command('init-db')
def init_db():
    click.Echo("Setting up database")
