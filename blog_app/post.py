class Post:
    def __init__(self, title, id, author, text, tag_line):

        if not isinstance(title, str):
            ValueError('title must be a string')
        self.title = title

        self.id = id

        if not isinstance(author, str):
            ValueError('author must be a string')
        self.author = author

        if not isinstance(text, str):
            ValueError('text must be a string')
        self.text = text

        if not isinstance(tag_line, str):
            ValueError('tag_line must be a string')
        self.tag_line = tag_line

    def __str__(self):
        return f'{self.title} {self.author}'
    
    def from_json(data):
        if not isinstance(data,dict):
            raise TypeError()
        
        return Post(data['title'],data['id'],data['author'],data['text'],data['tag_line'])


class ImagePost(Post):
    def __init__(self, title, id, author, text, tag_line, images, path):
        super().__init__(title, id, author, text, tag_line)
        self.images = images
        self.path = path
        self.__index = 0

    def __iter__(self):
        return self

    def __next__(self):
        a = self.images[self.__index]
        self.__index = self.__index + 1
        if self.__index == len(self.images):
            raise StopIteration
        return a
