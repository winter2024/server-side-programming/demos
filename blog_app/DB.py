import oracledb

from blog_app.user import User
from .post import Post
from flask import g, current_app
from .fakeDB import FakeDB
import os


def get_db():

    if current_app.testing:
        g.db = FakeDB()
    else:
        if 'db' not in g:
            g.db = Database()
    return g.db


def close_db(toto):
    db = g.pop('db', None)
    if db is not None:
        db.close()


class Database:
    def __init__(self, autocommit=True):
        self.__connection = self.__connect()
        self.__connection.autocommit = autocommit

    def run_file(self, file_path):
        statement_parts = []
        with self.__connection.cursor() as cursor:
            with open(file_path, 'r') as f:
                for line in f:
                    statement_parts.append(line)
                    if line.strip('\n').strip('\n\r').strip().endswith(';'):
                        statement = "".join(
                            statement_parts).strip().rstrip(';')
                        if statement:
                            try:
                                cursor.execute(statement)
                            except Exception as e:
                                print(e)
                        statement_parts = []

    def get_posts(self):
        posts = []
        cursor = self.__get_cursor()
        results = cursor.execute(
            'select title, post_id, author, text, tag_line from blog_posts')
        for row in results:
            post = Post(row[0], row[1], row[2], str(row[3]), row[4])
            posts.append(post)
        return posts

    def get_post(self, id):
        post = None
        cursor = self.__get_cursor()
        cursor.execute(
            'select title, post_id, author, text, tag_line from blog_posts where post_id=:id', id=id)
        row = cursor.fetchone()
        if row:
            post = Post(row[0], row[1], row[2], str(row[3]), row[4])
        return post

    def get_post_by_title(self, title):
        post = None
        cursor = self.__get_cursor()
        cursor.execute(
            'select title, post_id, author, text, tag_line from blog_posts where title=:title', title=title)
        row = cursor.fetchone()
        if row:
            post = Post(row[0], row[1], row[2], str(row[3]), row[4])
        return post

    def create_post(self, post):
        if (not isinstance(post, Post)):
            raise TypeError("expected Post object")

        with self.__get_cursor() as cursor:
            cursor.execute('insert into blog_posts (title, author, text, tag_line) values (:title, :author, :text, :tag_line)',
                           title=post.title,
                           author=post.author,
                           text=post.text,
                           tag_line=post.tag_line)

    def create_user(self, user):
        if (not isinstance(user, User)):
            raise TypeError("expected USer object")

        with self.__get_cursor() as cursor:
            cursor.execute('insert into blog_users (email, password, username) values (:email, :password, :name)',
                           email=user.email,
                           password=user.pwd,
                           name=user.name)
    def get_user_by_email(self,email):
        user = None
        cursor = self.__get_cursor()
        cursor.execute(
            'select user_id,email,username,password from blog_users where email=:email', email=email)
        row = cursor.fetchone()
        if row:
            user = User(row[0],row[1], row[2], row[3])
            
        return user
    
    def get_user_by_id(self,id):
        user = None
        cursor = self.__get_cursor()
        cursor.execute(
            'select user_id,email,username,password from blog_users where user_id=:id', id=id)
        row = cursor.fetchone()
        if row:
            user = User(row[0],row[1], row[2], row[3])
            
        return user
    
    def close(self):
        '''Closes the connection'''
        if self.__connection is not None:
            self.__connection.close()
            self.__connection = None

    def __get_cursor(self):
        for i in range(3):
            try:
                return self.__connection.cursor()
            except Exception as e:
                # Might need to reconnect
                self.__reconnect()

    def __reconnect(self):
        try:
            self.close()
        except oracledb.Error as f:
            pass
        self.__connection = self.__connect()

    def __connect(self):
        return oracledb.connect(user=os.environ['DBUSER'], password=os.environ['DBPWD'],
                                host="198.168.52.211", port=1521, service_name="pdbora19c.dawsoncollege.qc.ca")


if __name__ == '__main__':
    print('Provide file to initialize database')
    file_path = input()
    if os.path.exists(file_path):
        db = Database()
        db.run_file(file_path)
        db.close()
    else:
        print('Invalid Path')
