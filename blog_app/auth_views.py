from flask import Blueprint, flash, redirect, render_template, request, url_for
from werkzeug.security import check_password_hash, generate_password_hash
from blog_app.forms import LoginForm, SignupForm
from blog_app.user import User
from blog_app.DB import get_db
from flask_login import login_required, login_user, logout_user
bp = Blueprint('auth', __name__, url_prefix='/auth/')


@bp.route('signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()

    if request.method == 'POST' and form.validate_on_submit():
        pwd_hash = generate_password_hash(form.password.data)
        user = User(form.email.data, form.name.data, pwd_hash)
        get_db().create_user(user)

        return redirect(url_for('home.index'))
    return render_template('signup.html', form=form)


@bp.route('login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        user = get_db().get_user_by_email(form.email.data)
        if check_password_hash(user.pwd, form.password.data):
            login_user(user, remember=False)
            return redirect(url_for('home.index'))
        else:
            flash('Incorrect info')

    return render_template('login.html', form=form)

@bp.route('logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))