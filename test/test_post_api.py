import unittest
from blog_app import create_app
from blog_app.fakeDB import FakeDB


class TestPostApi(unittest.TestCase):

    def setUp(self):
        app = create_app(True)
        app.config.update(TESTING=True)
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_get_posts(self):
        result = self.client.get('/api/posts/')
        self.assertEqual(200, result.status_code)
        self.assertIsNotNone(result.json)
        self.assertEqual(1, len(result.json))

    def test_get_post(self):
        result = self.client.get('/api/posts/?title=first post')
        self.assertEqual(200, result.status_code)
        self.assertIsNotNone(result.json)
        self.assertEqual('first post', result.json['title'])

    def test_get_post_error(self):
        result = self.client.get('/api/posts/?name=test')
        self.assertEqual(400, result.status_code)

        result = self.client.get('/api/posts/?title=test')
        self.assertEqual(404, result.status_code)

    def test_create_post(self):
        data = {
            "author": "ddubois",
            "id": "2",
            "tag_line": "testing",
            "text": "this is a test",
            "title": "test post"
        }
        result = self.client.post('/api/posts/', json=data)
        self.assertEqual(200, result.status_code)

    if __name__ == '__main__':
        unittest.main()
