#!/bin/bash
#command for linting py files
pycodestyle blog_app/*.py --ignore=E501 --show-source > lintreport.txt

#linting jinja
djlint blog_app/templates/ --profile=jinja --ignore "H005,H030,H031" >> lintreport.txt 

